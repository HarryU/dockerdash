(ns dockerdash.handler
  (:require [compojure.core :refer :all]
            [unixsocket-http.core :as uhttp]
            [compojure.route :as route]
            [cheshire.core :as json]
            [hiccup.core :refer :all]
            [hiccup.util :refer :all]
            [hiccup.table :refer :all]
            [hiccup.element :refer :all]
            [hiccup.page :refer :all]
            [clojure.string :as str]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]))

(def client (uhttp/client "unix:///var/run/docker.sock"))

(defn ps []
  (json/parse-string ((uhttp/get client "/containers/json") :body) true))

(defn container-names [status]
  (map :Names status))

(defn fmt-name [s]
  (clojure.string/replace (first s) "/" ""))

(defn container-names-fmt [status]
  (map #(fmt-name %) (container-names status)))

(defn container-status [status]
  (map :Status status))

(defn logs [cname]
  (-> client (uhttp/get (str "/containers/" cname "/logs?stdout=1&stderr=1&tail=10")) :body))

(defn format-logs [logs]
  (str/replace (str/replace logs #".\000\000\000...." "") "\n" "<br>"))

(defn link-table [label-key val]
  (if (= 0 label-key) [:a {:href (str "/" val)} val] val))

(defn make-table [s]
  (html [:div.centre (to-table1d (map vector
			 (container-names-fmt s)
			 (container-status s))
		    [0 "Name" 1 "Status"]
		    {:table-attrs {:class "centre"}
		     :data-value-transform link-table})]))

(def base-page
  (html [:head
	 (include-css "https://unpkg.com/mvp.css@1.12/mvp.css")
	 [:style ".centre {text-align: center;}"]]))

(defroutes app-routes
  (GET "/" []
       (html (concat base-page (make-table (ps)))))
  (GET "/:cname" [cname]
       (concat base-page (html [:p  (format-logs (logs cname))])))
  (route/not-found "Not Found"))

(def app
  (wrap-defaults app-routes site-defaults))
